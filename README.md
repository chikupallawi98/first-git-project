# git account

    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"


# git commands

    git status // To know the status of changes

    git checkout -b <branch-name> // to create a new branch with name branch-name

    git checkout <branch-name> // to move to another branch with name branch-name

    git checkout - // to move to previous branch 

    git add <file-name>  // add file to stage

    git add . //  add all files to stage

    git add -A //  add all files to stage

    git commit -m <commit message> //  add the commit msg for this commit

    git push -u origin <branch-name>  //  create a new branch in origin with name branch-name and push all the local changes to origin 

    git push origin // push all the local changes to origin
    

